import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { StyleSheet } from 'react-native';
import MeuComponente from './MeuComponente';
import ComponenteImagem from './ComponenteImagem';


const Stack = createStackNavigator()

export default function App() {

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: "red"
          },
          headerTintColor: "white"
        }}
        initialRouteName="Inicio" >
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name="Inicio" component={MeuComponente} />
        <Stack.Screen
          options={{
            title: "Pokemons"
          }}
          name="Imagem" component={ComponenteImagem} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
