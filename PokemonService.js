export default class PokemonService {
    static listarPokemons = () => {
        return fetch('https://api.pokemon.com/us/api/pokedex/kalos', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then((response) => response.json());
    }
}