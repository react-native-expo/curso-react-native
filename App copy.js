import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';

export default class App extends Component {

  state = {
    nome: "Nataniel"
  }

  mudarNome = () =>{
    this.setState({nome:"Outro Nome"})
  }

  onChangeText = (nome) => {
    this.setState({nome})
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>{this.state.nome}</Text>
        <TextInput value={this.state.nome} onChangeText={this.onChangeText} />
        <Button title="Mudar Nome" onPress={this.mudarNome} />
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
