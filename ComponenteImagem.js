import React, { Component } from "react";
import { View, Image, Text, StyleSheet, ScrollView } from "react-native";
import PokemonService from "./PokemonService";


export default class ComponenteImagem extends Component {

    constructor(props) {
        super(props)
        const { texto } = this.props.route.params.params
        this.state = {
            texto,
            lista: []
        }
        PokemonService.listarPokemons().then(
            (lista) => {
                this.setState({ lista })
            }
        )
    }

    render() {

        return (
            <ScrollView style={styles.container}>
                {this.state.lista.map((pokemon, key) => {
                    return (
                        <View key={key} style={styles.rowStyle}>
                            <View style={styles.textStyle}>
                                <Text style={{ fontSize: 22, textAlign: "center" }}>
                                    {pokemon.name}
                                </Text>
                            </View>
                            <View style={styles.imageStyle}>
                                <Image
                                    style={{ width: 90, height: 50 }}
                                    source={{uri:pokemon.ThumbnailImage}}
                                />
                            </View>
                        </View>
                    )
                })}

            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    rowStyle: {
        flexDirection: 'row',
        marginTop: 10
    },
    textStyle: {
        width: "50%", alignSelf: 'flex-start', marginStart: 20
    },
    imageStyle: {
        width: "50%", alignItems: 'center', justifyContent: 'center'
    }
});
